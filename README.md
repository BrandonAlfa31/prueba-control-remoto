# Prueba Control Remoto

## Description
This is a test about the design of a PCB, the PCB works as a controller of diferents aplications.

## Visuals
Visuals if neededa

## Usage
La PCB hará diferentes cosas.

## Support
c.araya.j19@gmail.com

## Road map
This section describes the features and status of the current version of the project

### Future implementations:
  * Future implementation 1
  * Future implementation 2

## Authors and acknowledgment
  * Carlos Araya Jiménez (c.araya.j19@gmail.com)

## License
This project is licensed under GPLv3+ (GNU General Public License version 3 or later).

## Project status
Active (2023.september.11)
